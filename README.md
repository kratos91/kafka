## Consumidor y productor en python para Apache kafka

### CORRER ZOOKEEPER Y KAFKA CON DOCKER
	docker-compose -f docker-compose.yml up -d
### EJECUTAR TERMINAL DE KAFKA
	docker exec -it kafka /bin/sh

### Crear nuevo topico
	kafka-topics.sh --create --topic test-topic --bootstrap-server localhost:9092 --replication-factor 1 --partitions 4
### Describir topico
	kafka-topics.sh --describe --topic test-topic --bootstrap-server localhost:9092
### Listar topicos
	kafka-topics.sh --list --bootstrap-server localhost:9092

### Unirse como consumidor a un topico
	kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic nombre_topico --group nombre_grupo

### Crear productor en un topico
	kafka-console-producer.sh --broker-list localhost:9092 --topic nombre_topico
